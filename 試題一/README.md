# DA_Project

作者：楊婷文
<br/>
時間：2024/03/20

[Toc]

<div style="page-break-after: always;"></div>

## 資料處理

### 資料集

* **資料集為客戶明細檔，記錄個別客戶身分標籤，及詳細資訊。**
    * 多對多關係
    * detail_value類型為數字與文字混合

| member_id | tag_name | detail_name | detail_value |
| ---- | ---- | ---- | ---- |
| A | X | 問題單號 | AAA |
| A | Y | 官網登入次數 | 10 |
| B | Z | 官網登入次數 | 15 |

### 目標 

* 將CSV檔轉成Json並輸出。
* Key：id非唯一，需要進行轉換。
* Value：tag_name、detail_name、detail_value 降階


### 概念

```mermaid

flowchart LR
classDef subbak fill:#F5F5F5, stroke:#C0C0C0
classDef other fill:#FFE4E1, stroke:#DC143C
classDef DTSA fill:#F5F5F5, stroke:#F5F5F5
classDef DTSB fill:#F5F5F5, stroke:#F5F5F5

    subgraph layer1
    direction LR    
    A("- groupby ID and TagName -")
        subgraph layer2
        direction TB
        B("- groupby ID -")
        end
    end

    CSV匯入 --> layer1 --> Json匯出

class layer1 subbak
class layer2 subbak
class A DTSA
class B DTSB

```

<div style="page-break-after: always;"></div>

## 執行步驟
### 1. 匯入資料

```python

import numpy as np
import pandas as pd
import csv as csv
import json
import pprint

## RAW DATA
file_path = 'G:\\我的雲端硬碟\\DA_Project\\CSV2JSON.csv'
df = pd.DataFrame(pd.read_csv(file_path, header = 0))
df.head(5)

```
### 2. GroupbyID_and_TagName

```python 
## Add _id column
df_id = pd.DataFrame(data = df['member_id'], \
                    columns = ['member_id']).rename(columns = {'member_id': '_id'})
df_id

## reset_index and join df & df_id
df = df_id.reset_index(drop = True).join(df)
df

## layer 1 
df_1_groupby_ID_and_TagName = \
    df.groupby(['_id', 'member_id', 'tag_name'])[['detail_name', 'detail_value']]\
        .apply(lambda x: x.to_dict(orient = 'records'))

df_2_ResetIndex_and_Rename = \
    df_1_groupby_ID_and_TagName.reset_index()\
                                .rename(columns = {0:'detail'})
```

### 3. GroupbyID_ & 轉換型態
```python 
## layer 2 
df_3_groupby_ID_and_Rename = \
    df_2_ResetIndex_and_Rename.groupby(['_id', 'member_id'])[['tag_name', 'detail']]\
                                .apply(lambda x: x.to_dict(orient = 'records'))\
                                                        
df_4_ResetIndex_and_Rename =  \
    df_3_groupby_ID_and_Rename.reset_index()\
                                .rename(columns = {0:'tags'})

type(df_4_ResetIndex_and_Rename)

## DataFrame to dict 
df_5_To_Dict = df_4_ResetIndex_and_Rename.to_dict(orient = 'records')
```
### 4. 匯出
```python 
## Print_Output                                                             
Print_Output = pprint.PrettyPrinter(indent = 4)
Print_Output.pprint(df_5_To_Dict)

```
### 5. 寫入檔案

```python 

## write Output to .json
with open('G:\\我的雲端硬碟\\DA_Project\\Output_data.json', \
            'w', encoding = 'utf-8') as f:
    json.dump(df_5_To_Dict, f, indent = 4, ensure_ascii = False) 

```
### 輸出格式(Output_data.json)

```latex
[
    {
        "_id": "D2955653614",
        "member_id": "D2955653614",
        "tags": [
            {
                "tag_name": "一般會員",
                "detail": [
                    {
                        "detail_name": "官網登入次數",
                        "detail_value": "20"
                    },
                    {
                        "detail_name": "銷售網站登入次數",
                        "detail_value": "3"
                    }
                ]
            },
            {
                "tag_name": "三年客訴",
                "detail": [
                    {
                        "detail_name": "問題單號",
                        "detail_value": "gEw47233363"
                    }
                ]
            }
        ]
    },
    {
        "_id": "D8648642537",
        "member_id": "D8648642537",
        "tags": [
            {
                "tag_name": "一般會員",
                "detail": [
                    {
                        "detail_name": "官網登入次數",
                        "detail_value": "27"
                    },
                    {
                        "detail_name": "銷售網站登入次數",
                        "detail_value": "9"
                    }
                ]
            }
        ]
    },
    ...
]
```


## 上傳至GitLab

 * **Step1. 地端資料夾創建 .git 目錄**

```mermaid

flowchart LR

a["Working<br>Directory"] --> 
b["git<br>init"] --> c["git<br>add"] --> d["git<br>commit"] --> 
e["git<br>push"] --> f["git<br>fetch"] --> g["git<br>clone"] --> 
a["Working<br>Directory"]

```
<!--
<br />
<br />
<br />
<div align = center>
<img src="C:\Users\wen\Desktop\DA_Project\試題一\step1_建目錄.PNG" width="80%" />
</div>

<div style="page-break-after: always;"></div>
-->

 * **Step2. Commit 至本地資料庫 (Local Repository)**
<!--
<div align = center>
<img src="C:\Users\wen\Desktop\DA_Project\試題一\step2_local.PNG" width="80%" />
</div>

<br />
<br />
<br />
-->
 * **Step3. Push 至遠端資料庫 (Remote Repository)**
<!--
<div align = center>
<img src="C:\Users\wen\Desktop\DA_Project\試題一\step3_remote.PNG" width="80%" />
</div>
-->

