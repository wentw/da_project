# SCRIPT

import numpy as np
import pandas as pd
import csv as csv
import json
import pprint

## RAW DATA
file_path = 'G:\\我的雲端硬碟\\DA_Project\\CSV2JSON.csv'
df = pd.DataFrame(pd.read_csv(file_path, header = 0))
df.head(5)

## Add _id column
df_id = pd.DataFrame(data = df['member_id'], columns = ['member_id']).rename(columns={'member_id': '_id'})
df_id

## reset_index and join df & df_id
df = df_id.reset_index(drop = True).join(df)
df

## layer 1 
df_1_groupby_ID_and_TagName = df.groupby(['_id', 'member_id', 'tag_name'])[['detail_name', 'detail_value']]\
                                .apply(lambda x: x.to_dict(orient = 'records'))

df_2_ResetIndex_and_Rename = df_1_groupby_ID_and_TagName.reset_index()\
                                                        .rename(columns = {0:'detail'})


## layer 2 
df_3_groupby_ID_and_Rename = df_2_ResetIndex_and_Rename.groupby(['_id', 'member_id'])[['tag_name', 'detail']]\
                                                        .apply(lambda x: x.to_dict(orient='records'))\
                                                        
df_4_ResetIndex_and_Rename = df_3_groupby_ID_and_Rename.reset_index()\
                                                        .rename(columns = {0:'tags'})

type(df_4_ResetIndex_and_Rename)

## DataFrame to dict 
df_5_To_Dict = df_4_ResetIndex_and_Rename.to_dict(orient = 'records')

## Print_Output                                                             
Print_Output = pprint.PrettyPrinter(indent = 4)
Print_Output.pprint(df_5_To_Dict)

## write Output to .json
with open('G:\\我的雲端硬碟\\DA_Project\\Output_data.json', 'w', encoding = 'utf-8') as f:
    json.dump(df_5_To_Dict, f, indent = 4, ensure_ascii = False) 




